<?php

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$obj = new Animal("shaun");
echo "Name : $obj->name<br>";
echo "legs : $obj->legs<br>";
echo "cold blooded : $obj->cold_blooded<br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name<br>";
echo "legs : $kodok->legs<br>";
echo "cold blooded : $kodok->cold_blooded<br>";
echo "Jump : $kodok->jump<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name<br>";
echo "legs : $sungokong->legs<br>";
echo "cold blooded : $sungokong->cold_blooded<br>";
echo "Yell : $sungokong->yell<br><br>";

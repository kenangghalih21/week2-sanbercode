<?php

class Animal
{

  public $name;
  public $legs = 4;
  public $cold_blooded = "no";

  public function __construct($string)
  { //construct adalah fungsi yg paling pertama dipanggil sebelum fungsi yg lain
    $this->name = $string;
  }
}

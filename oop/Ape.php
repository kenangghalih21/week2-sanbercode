<?php

require_once("Animal.php");

class Ape extends Animal
{
  public $legs = 2;
  public $yell = "Auoo";
  public function yell($aksi)
  {
    $this->yell = $aksi;
  }
}
